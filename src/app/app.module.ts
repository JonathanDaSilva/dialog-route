import {MatDialogModule} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

class Test implements ErrorHandler {
  handleError(error: any): void {
    console.error('handler', error)
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: ErrorHandler, useClass: Test},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
