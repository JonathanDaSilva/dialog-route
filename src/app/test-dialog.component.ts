import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {startWith} from 'rxjs/operators';

@Component({
  template: `
    <h1 mat-dialog-title>Test Dialog</h1>
    <div mat-dialog-content>
      <p>What's your favorite animal?</p>
      <mat-checkbox class="example-margin" [formControl]="control">Checked</mat-checkbox>
    </div>
    <div mat-dialog-actions>
      <button mat-button>No Thanks</button>
      <button mat-button [routerLink]="['../']">Ok</button>
    </div>
  `,
})
export class TestDialogComponent {
  public control = new FormControl(true);

  public canDeactivate(): Observable<boolean> {
    return this.control.valueChanges.pipe(
      startWith(this.control.value),
    );
  }
}
