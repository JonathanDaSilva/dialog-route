import { Component } from '@angular/core';

@Component({
  template: `
    <h1>Test</h1>
    <a [routerLink]="['/dialog']">Dialog</a>
    
    <router-outlet></router-outlet>
  `,
})
export class TestComponent {
}
