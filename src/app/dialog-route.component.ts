import {Component, OnDestroy} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, ReplaySubject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  template: ``,
})
export class DialogRouteComponent implements OnDestroy {
  public onDestroy$  = new ReplaySubject(1);
  private dialogRef: MatDialogRef<any, any>;

  public constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.dialogRef = this.dialog.open(
      this.route.snapshot.data.dialog,
      {
      disableClose: true,
        closeOnNavigation: false,
      }
    );

    this.dialogRef.backdropClick().pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.router.navigate(['../'], { relativeTo: this.route });
    });

    this.dialogRef.keydownEvents().pipe(takeUntil(this.onDestroy$)).subscribe((event) => {
      if (event.keyCode !== 27) {
        return;
      }
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

  public canDeactivate(): Observable<boolean> {
    return this.dialogRef.componentInstance.canDeactivate();
  }

  public ngOnDestroy() {
    this.dialogRef.close();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
