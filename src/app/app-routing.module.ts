import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule, MatDialogModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Routes, RouterModule, CanDeactivate, UrlTree} from '@angular/router';
import {take, tap} from 'rxjs/operators';
import {DialogRouteComponent} from 'src/app/dialog-route.component';
import {TestDialogComponent} from 'src/app/test-dialog.component';
import {TestComponent} from 'src/app/test.component';

class TestGuard implements CanDeactivate<DialogRouteComponent> {
  canDeactivate(component: DialogRouteComponent): Promise<boolean | UrlTree>{
    return component.canDeactivate().pipe(take(1), tap(console.warn.bind(console))).toPromise();
  }

}

const routes: Routes = [
      {
        path: '',
        component: TestComponent,
        children: [
          {
            path: 'dialog',
            component: DialogRouteComponent,
           data: { dialog: TestDialogComponent },
           canDeactivate: [TestGuard]
          },
        ],
    }
];

@NgModule({
  declarations: [
    TestComponent,
    DialogRouteComponent,
    TestDialogComponent,
  ],
  entryComponents: [
    TestDialogComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
  ],
  exports: [
    RouterModule,
  ],
  providers: [
    TestGuard,
  ]
})
export class AppRoutingModule { }
